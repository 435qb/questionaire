import { createRouter, createWebHashHistory } from 'vue-router'
const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/login.vue')
  },
  {
    path: '/test',
    name: 'Test',
    component: () => import('@/views/newUI/test.vue')
  },
 {
    path: '/register',
    name: 'Register',
    component: () => import('@/views/register/index.vue')
  },
  {
    path: '/loginWithConfirm',
    name: 'LoginCon',
    component: () => import('@/views/login/loginWithConfirm.vue')
  },
  {
    path: '/v2/admin',
    component:()=>import('@/views/newUI/Admin/admin.vue'),
    children:[
      {
        path: '',
        component: ()=>import('@/views/newUI/Admin/preview.vue'),
      },
      {
        path: '/test',
        component: ()=>import('@/views/newUI/Admin/testEX.vue'),
      },
      {
        path: 'board',
        component: ()=>import('@/views/newUI/Admin/board.vue'),
      },
      {
        path: 'adv',
        component: ()=>import('@/views/newUI/Admin/advertise.vue'),
      },
      {
        path: 'view',
        component: ()=>import('@/views/newUI/Admin/preview.vue'),
      },
      {
        path: 'list',
        component: ()=>import('@/views/newUI/Admin/tenantlist.vue'),
      },
      {
        path: 'bill',
        component: ()=>import('@/views/newUI/Admin/bill.vue'),
      },
      {
        path: 'edit',
        component: ()=>import('@/views/newUI/Tenant/editselfinfo.vue'),
      },
    ],
  },
  {
    path: '/v2/tenant',
    component:()=>import('@/views/newUI/Tenant/tenant.vue'),
    children:[
      {
        path: '',
        component: ()=>import('@/views/newUI/Tenant/viewselfinfo.vue'),
      },
      {
        path: 'editConsumer',
        component: ()=>import('@/views/newUI/Consumer/editselfinfo.vue'),
      },
      {
        path: 'self',
        component: ()=>import('@/views/newUI/Tenant/viewselfinfo.vue'),
      },
      {
        path: 'editself',
        component: ()=>import('@/views/newUI/Tenant/editselfinfo.vue'),
      },
      {
        path: 'consumer',
        component: ()=>import('@/views/newUI/Tenant/consumerlist.vue'),
      },
      {
        path: 'ctree',
        component: ()=>import('@/views/newUI/Tenant/consumertree.vue'),
      },
      {
        path: 'deposit',
        component: ()=>import('@/views/newUI/Tenant/deposit.vue'),
      },
      
    ],
  },
  {
    path: '/v2/consumer',
    component:()=>import('@/views/newUI/Consumer/consumer.vue'),
    children:[
      {
        path: '',
        component: ()=>import('@/views/newUI/Consumer/answererlist.vue'),
      },
      {
        path: 'editAnswer',
        component: ()=>import('@/views/newUI/answerer/editselfinfo'),
      },
      {
        path: 'ans',
        component: ()=>import('@/views/newUI/Consumer/answererlist.vue'),
      },
      {
        path: 'analysis',
        component: ()=>import('@/views/newUI/Consumer/Questionnaire/analysis.vue'),
      },
      {
        path: 'create',
        component: ()=>import('@/views/newUI/Consumer/Questionnaire/createquestionnaire.vue'),
      },
      {
        path: 'edit',
        component: ()=>import('@/views/newUI/Consumer/editQuestionnaire/createquestionnaire.vue'),
      },
      {
        path: 'preview',
        component: ()=>import('@/views/newUI/Consumer/previewQuestionnaire.vue'),
      },
      {
        path: 'group',
        component: ()=>import('@/views/newUI/Consumer/grouplist.vue'),
      },
      {
        path: 'import',
        component: ()=>import('@/views/newUI/Consumer/importanswerer.vue'),
      },
      {
        path: 'ques',
        component: ()=>import('@/views/newUI/Consumer/questionnairelist.vue'),
      },
      {
        path: 'list',
        component: ()=>import('@/views/newUI/Consumer/answererlist.vue'),
      },
      {
        path: 'view',
        component: ()=>import('@/views/newUI/Consumer/viewgroup.vue'),
      },
      {
        path: 'choose',
        component: ()=>import('@/views/newUI/Consumer/Questionnaire/choosequestionnaire.vue'),
      },
      {
        path: 'editans',
        component: ()=>import('@/views/newUI/answerer/editselfinfo.vue'),
      },
      
      {
        path: 'editself',
        component: ()=>import('@/views/newUI/Consumer/editselfinfo.vue'),
      },
    ],
  },
  {
    path: '/v2/answerer',
    component:()=>import('@/views/newUI/answerer/answerer.vue'),
    children:[
      {
        path: '',
        component: ()=>import('@/views/newUI/answerer/viewselfinfo.vue'),
      },
      {
        path: 'self',
        component: ()=>import('@/views/newUI/answerer/viewselfinfo.vue'),
      },
      {
        path: 'award',
        component: ()=>import('@/views/newUI/answerer/award.vue'),
      },
      {
        path: 'editself',
        component: ()=>import('@/views/newUI/answerer/editselfinfo.vue'),
      },
      {
        path: 'ques',
        component: ()=>import('@/views/newUI/answerer/questionnairelist.vue'),
      },
      {
        path: 'group',
        component: ()=>import('@/views/newUI/answerer/grouplist.vue'),
      },
      {
        path: 'ans',
        component: ()=>import('@/views/newUI/answerer/answerquestionnaire/answer.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes : routes
})

export default router
