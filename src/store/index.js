import { createStore } from 'vuex'
import app from '@/store/modules/app'
import getters from "@/store/modules/getters";
import questionnaire from "@/store/modules/questionnaire";
import idInfo from "@/store/modules/idInfo";

export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    app,
    questionnaire,
    idInfo
  }
})
