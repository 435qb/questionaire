import router from "@/router";

export default {
    namespaced: true,
    state: {
        userId: '',
        token: ''
    },
    mutations: {
        setUserId(state, userId) {
            // eslint-disable-next-line no-unused-expressions
            (state.userId=userId)
            localStorage.setItem("userId", userId);
        },
        setToken(state, token) {
            // eslint-disable-next-line no-unused-expressions
            (state.token=token)
            localStorage.setItem("token", token);
        },
    },
    actions: {
        editUserId({commit},userId){
            localStorage.removeItem("userId")
            commit("userId",userId);
        },
        getUserId(){
            return localStorage.getItem("userId")
        },
    },
    getters: {
        userId: state =>{
            return state.userId
        }
    }
};
