// 提供token存储和页面跳转 ，这里将login逻辑写到这里 并在login.vue中引用
import router from "@/router";

export default {
    namespaced: true,
    state: {
        state0: 1,
        datas: [],
        title: null,
        temp: 1,
        uuid: ""
    },
    mutations: {
        setQuesData(state, data) {
            // eslint-disable-next-line no-unused-expressions
            (state.datas=data), localStorage.setItem("datas", data);
        },
        setTitle(state, title1) {
            // eslint-disable-next-line no-unused-expressions
            (state.title=title1), localStorage.setItem("title", title1);
        },
        setState0(state, state0) {
            // eslint-disable-next-line no-unused-expressions
            (state.state0=state0), localStorage.setItem("state0", state0);
        },
        setTemp(state, temp) {
            // eslint-disable-next-line no-unused-expressions
            (state.temp=temp), localStorage.setItem("temp", temp);
        },
        setUuid(state, uuid) {
            // eslint-disable-next-line no-unused-expressions
            (state.uuid=uuid), localStorage.setItem("uuid", uuid);
        },
    },
    actions: {
        editQuesData({commit},data){
            localStorage.removeItem("datas")
            commit("datas",data);
        },
        getDatas(){
            return localStorage.getItem("datas")
        },
        editTitle({commit},title1){
            localStorage.removeItem("title")
            commit("title",title1);
        },
        getTitle(){
            return localStorage.getItem("title")
        },
        getUuid(){
            return localStorage.getItem("uuid")
        }
    },
    getters: {
        datas: state =>{
            return state.datas
        }
    }
};
