import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ViewUIPlus from 'view-ui-plus'
import VueCookies from 'vue-cookies'

import '@/styles/index.scss'
import 'element-plus/dist/index.css'
//import '@/router/permission'
import '@/styles/style.css'
import '@/styles/simple-line-icons.css'
import '@/styles/font-awesome.min.css'

import 'view-ui-plus/dist/styles/viewuiplus.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)
                .use(VueCookies)
                .use(store)
                .use(router)
                .use(ViewUIPlus)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.mount('#app')
