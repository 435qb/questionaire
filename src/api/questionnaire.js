import request from "@/api/request";

export const createQuestionnaire = (data) =>{
    return request({
        url: '/questionnaire/create',
        method: 'POST',
        data
    })
}

export const getQuestionnaire = (data) =>{
    return request({
        url: '/questionnaire/answerQuestionnaire',
        method: 'POST',
        data
    })
}

export const submitQuestionnaire=(data)=>{
    return request({
        url: 'submitAnswer',
        method: 'POST',
        data
    })
}


export  const editQuestionnaire=(data)=>{
    return request({
        url: '/questionnaire/allquestions',
        method: 'POST',
        data
    })
}

export const preview=(data)=>{
    return request({
        url: '/questionnaire/allquestions',
        method: 'POST',
        data
    })
}