import { APISchema } from '@/api/type'
import { attachAPI, service } from "@/api/request";

interface TestAPISchema extends APISchema {
    getImagePath: {
        request: {
            "userUuid": string,
        },
        response: {
            "format"?: string,
            "image_uuid"?: string,
        },
    },
    changeImage: {
        request: {
            "userUuid": string,
            "imageUuid": string

        }
        response: {
        },
    },
    getAll: {
        request: {
            "uuid": string,
            "role_id": number
        }
        response: {
            "valid": number,
            "address": string,
            "role": string,
            "organization": string,
            "name": string,
            "cite": number,
            "id": number,
            "tag": string,
            "uuid": string,
            "age": number

        },
    }
}
export const api = attachAPI<TestAPISchema>(service, {
    getImagePath: '/user/getImageName',
    changeImage: '/user/changeImage',
    getAll: '/user/getUserInfo',
});