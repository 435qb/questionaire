import { APISchema } from '@/api/type'
import { attachAPI, service } from "@/api/request";

interface TestAPISchema2 extends APISchema {
    query: {
        request: {
            "name": string,
            "pageNum": number,
            "pageSize": number,
            "uuid": string,
        },
        response: {
            "pageNum": number,
            "pageSize": number,
            "size": number,
            "startRow": number,
            "endRow": number,
            "total": number,
            "pages": number,
            "list": Array<{
                "valid": number,
                "questionnaire_max":number,
                "address": string,
                "cluster_max": number,
                "name": string,
                "cluster_number":number,
                "id":number,
                "questionnaire_number": number,
                "uuid": string
            }>,
            "prePage": number,
            "nextPage": number,
            "isFirstPage": boolean,
            "isLastPage": boolean,
            "hasPreviousPage": boolean,
            "hasNextPage": boolean,
            "navigatePages": number,
            "navigatepageNums": Array<number>
            "navigateFirstPage": number,
            "navigateLastPage": number,
            "lastPage": number,
            "firstPage": number,
        },
    },
    queryTenant:{
        request: {
            "uuid": string
        },
        response: {
            "valid":number,
            "address": string,
            "balance": number,
            "organization": string,
            "name": string,
            "phonenumber": number,
            "id": number,
            "brief_introduction": string,
            "uuid": string,
            "age": true,
            "totalcost": number

        },
    }
    updateInfo: {
        request: {
            "uuid": string
        },
        response: {
        },
    }
    selectNotice:{
        request: {
        },
        response: {
            "list": Array<{
                "valid":number,
                "date":string,
                "id": number,
                "title": string,
                "content": string
            }>,
        },
    },
    createConsumer:{
        request: {
            username: string,
            password: string,
            name: string,
            phone: string,
            address: string,
            tenant_uuid: string
        },
        response: {
            "message": string,
            "success":number
        },
    },
    rechargeConsumer:{
        request: {
            "tenant_uuid":string,
            "consumer_uuid":string,
            "money":number,
            "type":number,
            "number":number
        },
        response: {
        },
    },
    canDeleteTheConsumer:{
        request: {
            "uuid": string,
        },
        response: {
            "status": number,
        },
    },
    deleteConsumer: {
        request: {
            "uuid": string,
        },
        response: {
        },
    },
    getSingleInfoByUUID: {
        request: {
            "uuid":string
        },
        response: {
            "password": string,
            "role_id": number,
            "phone": string,
            "id": number,
            "uuid": string,
            "username": string
        }
    }
}
export const api = attachAPI<TestAPISchema2>(service, {
    query: '/consumer/queryByName',
    queryTenant: '/tenant/getInfoByUuid',
    updateInfo:'/tenant/change',
    selectNotice:'/tenant/selectNotice',
    createConsumer:'/tenant/createConsumer',
    rechargeConsumer : '/tenant/rechargeConsumer',
    canDeleteTheConsumer:'/consumer/queryStatus',
    deleteConsumer:'/consumer/delete',
    getSingleInfoByUUID:'/user/findInfoByUUID'
});