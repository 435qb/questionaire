import { APISchema } from '@/api/type'
import { attachAPI, service } from "@/api/request";

interface TestAPISchema extends APISchema {
    query: {
        request: {
            "name": string,
            "pageNum": number,
            "pageSize": number,
            "uuid": number
        },
        response: {
            "pageNum": number,
            "pageSize": number,
            "size": number,
            "startRow": number,
            "endRow": number,
            "total": number,
            "pages": number,
            "list": Array<{
                "valid": number
                "created_time": string,
                "number": number,
                "name": string,
                "history_number": string,
                "id": number,
                "uuid": string
            }>,
            "prePage": number,
            "nextPage": number,
            "isFirstPage": boolean,
            "isLastPage": boolean,
            "hasPreviousPage": boolean,
            "hasNextPage": boolean,
            "navigatePages": number,
            "navigatepageNums": Array<number>
            "navigateFirstPage": number,
            "navigateLastPage": number,
            "lastPage": number,
            "firstPage": number,
        },
    },
    queryques: {
        request: {
            "uuid": string,
        },
        response: Array<{
            "valid": number,
            "endDate": string,
            "createdBy": string,
            "name": string,
            "link": string,
            "id": number,
            "type": string,
            "uuid": string,
            "startDate": string,
        }>,
    },
    change: {
        request: {
            "uuid": string,
            "name": string,
        },
        response: {
        }
    },
    unadd: {
        request: {
            "consumer_uuid": string,
            "cluster_uuid": string,
        },
        response: Array<{
            "answerer_uuid": string,
            "valid": number,
            "address": string,
            "role": string,
            "organization": string,
            "name": string,
            "cite": number,
            "id": number,
            "tag": string,
            "uuid": string,
            "age": number,
            "consumer_uuid": string
        }>
    },
    getAnswerer: {
        request: {
            "uuid": string,
        },
        response: Array<{
            "answerer_uuid": string,
            "valid": number,
            "address": string,
            "role": string,
            "cluster_uuid": string,
            "organization": string,
            "name": string,
            "cite": number,
            "id": number,
            "tag": string,
            "uuid": string,
            "age": number,
        }>
    },
    addAnswererToCluster: {
        request: {
            "uuid": string,
            "answererList": Array<
                {
                    "uuid": string //答者uuid
                }
            >
        },
        response: Array<{
            "answerer_uuid": string,
            "valid": number,
            "cluster_uuid": string,
            "id": number
        }>
    },
    deleteAnswererFromCluster : {
        request: {
            "cluster_uuid": string,
            "answererList": Array<
                {
                    "uuid": string //答者uuid
                }
            >
        },
        response: {}
    },
    getAllAnswerer :{
        request: {
            "uuid":string,
        },
        response: Array<
        {
            "valid": number,
            "answerer_uuid": string,
            "address": string,
            "role": string,
            "organization": string,
            "name": string,
            "cite": number,
            "id": number,
            "tag": string,
            "uuid": string,
            "age": number,
            "consumer_uuid": string

        }
        >
    }
    changeTime : {
        request: {
            "questionnaire_uuid":string,
            "cluster_uuid":string,
            "start_date":string,
            "end_date":string
        },
        response: {}
    }
    getTime : {
        request: {
            "questionnaire_uuid":string,
            "cluster_uuid":string,
        },
        response: {
            "start_date":string,
            "end_date":string
        }
    },
    editSelfInfo : {
        request: {
            "uuid":string,
            "name":string,
            "username":string,
            "password":string,
            "address":string,
            "phone":string,
        },
        response: {
            "success":number,
            "message":string
        }
    },
    editAnswererInfo : {
        request: {
            "uuid":string,
            "name":string,
            "age":string,
            "address":string,
            "organization":string,
            "role":string,
            "tag":string
        },
        response: {
        }
    }
}
export const api = attachAPI<TestAPISchema>(service, {
    query: '/group/queryByName',
    queryques: '/questionnaire/getQuestionnaireByCluster',
    change: '/group/change',
    unadd: '/consumer/getAnswererNotAdd',
    getAnswerer: '/group/getAnswerer',
    addAnswererToCluster : '/group/addAnswererToCluster',
    deleteAnswererFromCluster : '/group/deleteAnswererFromCluster',
    getAllAnswerer : '/answerer/getAnswerer',
    changeTime : '/questionnaire/changeTime', 
    getTime : '/questionnaire/getQuestionnaireByUuid',
    editSelfInfo:'/consumer/changeBaseInfo',
    editAnswererInfo:'/answerer/changeAnswerByUUID'
});