import axios, { AxiosInstance, AxiosRequestHeaders, AxiosError } from 'axios';
import {
    APISchema,
    CreateRequestConfig,
    CreateRequestClient,
} from './type';

export function attachAPI<T extends APISchema>(
    client: AxiosInstance,
    apis: CreateRequestConfig<T>['apis'],
): CreateRequestClient<T> {
    const hostApi: CreateRequestClient<T> = Object.create(null);
    for (const apiName in apis) {
        const apiConfig = apis[apiName];
        let apiPath = apiConfig as string;

        hostApi[apiName] = (params) => {
            const _params = { ...(params || {}) };          
            let url = apiPath;
            console.log(url)
            return client.request({
                url,
                method: 'POST',
                data : params,
            });
        };
    }
    return hostApi;
}
import {ElMessage} from "element-plus";
export const service=axios.create({
    baseURL: "http://139.224.119.118:8085", //"http://139.224.119.118:8085" "http://localhost:8085"
    timeout : 5000,
    withCredentials : true,
})

service.interceptors.request.use(config=>{
    config.headers.Authorization = localStorage.getItem('token')
    return config
},error => {
    return Promise.reject(new Error(error))
})

service.interceptors.response.use((response)=>{

    const meta=response.data
    if(meta.code==="666"){
        // ElMessage.success(meta.message);
        return meta
    }else {
        ElMessage.error(meta.message)
        return  Promise.reject(new Error(meta.message))
    }
},error => {
    error.response && ElMessage.error(error.response.data)
    return Promise.reject(new  Error((error.response.data)))
})

export default service
