import { APISchema } from '@/api/type'
import { attachAPI, service } from "@/api/request";

interface TestAPISchema extends APISchema {
    answerer_status: {
        request: {
            "uuid": string
        },
        response: {
            "status": number
        },
    },
    answerer_delete: {
        request: {
            "consumer_uuid": string,
            "answerer_uuid": string
        },
        response: {
        },
    },
    answerer_checkFirst: {
        request: {
            "uuid": string,
        },
        response: {
            "jump": number //是否跳转 1跳转 0不跳转
        },
    },
    answerer_createAnswererBatch: {
        request: {
            "uuid": string,
            "answererList":
            {
                "username": string,
                "password": string,
                "phone": string,
                "name": string
            }[]
        },
        response:
        {
            "username": string,
            "password": string,
            "phone": string,
            "name": string
        }[],


    }
}
export const api = attachAPI<TestAPISchema>(service, {
    answerer_status: '/answerer/queryDelete',
    answerer_delete: '/answerer/deleteAnswerer',
    answerer_checkFirst: '/answerer/checkFirst',
    answerer_createAnswererBatch: 'answerer/createAnswererBatch'
});