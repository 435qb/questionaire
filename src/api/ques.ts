import { APISchema } from '@/api/type'
import { attachAPI, service } from "@/api/request";

interface TestAPISchema2 extends APISchema {
    query: {
        request: {
            "uuid": string,
        },
        response: Array<{
            "valid": number,
            "endDate": string,
            "name": string,
            "link": string,
            "id": number,
            "type": string,
            "uuid": string,
            "startDate": string,
        }>,
    },
}
export const api = attachAPI<TestAPISchema2>(service, {
    query: '/questionnaire/getQuestionnaireByCluster',
});