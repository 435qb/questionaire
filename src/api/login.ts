import { APISchema } from '@/api/type'
import { attachAPI, service } from "@/api/request";

interface TestAPISchema extends APISchema {
    login : { // fake
        request: {
            "username": string,
            "password": string,
        },
        response: {
            "roleId": number,
            "uuid": string,
        },
    },
    loginWithConfirm : {
        request: {
            phone: string,
            auth_Code: string,
        },
        response: {
            "roleId": number,
            "uuid": string,
        },
    },
    register: {
        request: {
            "username": string,
            "password": string,
            "phone": string,
            "auth_code": string,
            "role_id": number,
        },
        response: {
            "username": string,
            "password": string,
            "phone": string,
            "auth_code": string,
            "role_id": string,
            "uuid": string,
            "message": string,
            "success": number,
        },
    }
    getConfirmCode: {
        request: {
            "phone": string,
        },
        response: {}
    },
    CompareCode: {
        request: {
            "phone": string,
            "auth_code": string,
        },
        response: {
        }
    },
    changePassword: {
        request: {
            "phone": string,
            "password": string,
            "passwordConfirm": string
        },
        response: {}
    }
    getSingleInfoByUUID: {
        request: {
            "uuid":string
        },
        response: {
            "password": string,
            "role_id": number,
            "phone": string,
            "id": number,
            "uuid": string,
            "username": string
        }
    }
}
export const api = attachAPI<TestAPISchema>(service, {
    login : '/login',
    loginWithConfirm : '/sms/login',
    register: '/user/register',
    getConfirmCode: '/user/phoneVerification',
    CompareCode: '/user/codeVerification',
    changePassword: '/user/changePassword',
    getSingleInfoByUUID:'/user/findInfoByUUID'
});