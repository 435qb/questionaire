import { APISchema } from '@/api/type'
import { attachAPI, service } from "@/api/request";

interface TestAPISchema extends APISchema {
    query: {
        request: {
            "name": string,
            "pageNum": number,
            "pageSize": number,
        },
        response: {
            "pageNum": number,
            "pageSize": number,
            "size": number,
            "startRow": number,
            "endRow": number,
            "total": number,
            "pages": number,
            "list": Array<{
                "valid": number,
                "address": string,
                "balance": number,
                "organization": string,
                "name": string,
                "phonenumber": number,
                "id": number,
                "brief_introduction": string,
                "uuid": string,
                "sex": number,
                "birthday": string,
                "totalcost": number
            }>,
            "prePage": number,
            "nextPage": number,
            "isFirstPage": boolean,
            "isLastPage": boolean,
            "hasPreviousPage": boolean,
            "hasNextPage": boolean,
            "navigatePages": number,
            "navigatepageNums": Array<number>
            "navigateFirstPage": number,
            "navigateLastPage": number,
            "lastPage": number,
            "firstPage": number,
        },
    },
    queryCost: {
        request: {
            "tenant_uuid": string,
        },
        response: {
            "list": Array<{
                "cluster_number": number,
                "address": string,
                "cluster_max": number,
                "questionnaire_number": number,
                "type": number,
                "uuid": string,
                "consumer_uuid": string,
                "valid": number,
                "questionnaire_max": number,
                "number": number,
                "money": number,
                "tenant_uuid": string,
                "name": string,
                "time": string
            }>,
        },
    },
    change: {
        request: {
            name: string,
            sex: number,
            organization: string,
            phonenumber: string,
            address: string,
            birthday: string,
            briefIntroduction: string,
            uuid: string
        },
        response: {
        },
    },
    canDelete: {
        request: {
            "uuid": string,
        },
        response: {
            "status": number,
        },
    },
    delete: {
        request: {
            "uuid": string,
        },
        response: {
        },
    },
    createNotice: {
        request: {
            "title": string,
            "content": string,
        },
        response: {
        },
    },
    selectNotice: {
        request: {
        },
        response: {
            "list": Array<{
                "valid": number,
                "date": string,
                "id": number,
                "title": string,
                "content": string
            }>,
        },
    },
    getInfoByUuid: {
        request: {
            "uuid": string
        },
        response: {
            "birthday": string,
            "address": string,
            "sex": number,
            "phonenumber": string,
            "uuid": string,
            "createdtime": string,
            "valid": number,
            "balance": number,
            "organization": string,
            "name": string,
            "id": number,
            "brief_introduction": string
        },
    },
    getAllBill: {
        request: {
        },
        response: {
            "list": Array<{
                "id":number,
                "uuid":string,
                "pay":number,
                "give":number

            }>,
        },
    },
    addBill:{
        request: {
            "pay":number,
            "give":number
        },
        response: {
        },
    },
    deleteBill:{
        request: {
            "uuid":string
        },
        response: {
        },
    },
    editBill:{
        request: {
            "uuid":string,
            "pay":number,
            "give":number
        },
        response: {
        },
    },
}
export const api = attachAPI<TestAPISchema>(service, {
    query: '/tenant/queryByName',
    queryCost: '/tenant/selectAllocate',
    change: '/tenant/change',
    canDelete: '/tenant/queryStatus',
    delete: '/tenant/delete',
    createNotice: '/tenant/createNotice',
    selectNotice: '/tenant/selectNotice',
    getInfoByUuid: '/tenant/getInfoByUuid',
    getAllBill:'/package/getallpackages',
    addBill:'package/create',
    deleteBill:'package/delete',
    editBill:'package/change'
});