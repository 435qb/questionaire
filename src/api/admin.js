import request from "@/api/request";

export const query = (data) => {
    return request({
        url: '/tenant/queryByName',
        method: 'POST',
        data
    })
}