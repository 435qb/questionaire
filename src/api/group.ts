import { APISchema } from '@/api/type'
import { attachAPI, service } from "@/api/request";

interface TestAPISchema extends APISchema {
    group_status: {
        request: {
            "uuid": string
        },
        response: {
            "status": number
        },
    },
    group_delete: {
        request: {
            "uuid": string
        },
        response: {
        },
    },
    group_create:{
        request: {
            "uuid": string
            "name": string,
        },
        response: {
            "uuid": string
        },
    }
}
export const api = attachAPI<TestAPISchema>(service, {
    group_status: '/group/queryStatus',
    group_delete: '/group/delete',
    group_create: '/group/create'
});