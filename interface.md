# 任务队列
## 创建问卷
## 答者获取待答问卷
## 答者获取已加入群组
# 试试
## 可以上传头像后弹出确认框
# 租户
## 通过uuid查询租户信息
tenant/getInfoByUuid
### 输入
```json
{
    "uuid":"00acc4a1-98f2-fcb1-9f2d-147b69e4b369"
}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "birthday": "2022-11-17T14:56:08.000+0000",
        "address": "河南省",
        "sex": 0,
        "phonenumber": "15038993296",
        "uuid": "00acc4a1-98f2-fcb1-9f2d-147b69e4b369",
        "createdtime": "2022-11-21T14:56:05.000+0000",
        "totalcost": 10.00,
        "valid": 1,
        "balance": 60,
        "organization": "东北大学",
        "name": "侯",
        "id": 100,
        "brief_introduction": "我来自东北大学软件学院"
    },
    "message": "查询成功"
}
```
## 租户给用户分配群组和问卷次数
tenant/rechargeConsumer
### 输入
```json
{
    "tenant_uuid":"00acc4a1-98f2-fcb1-9f2d-147b69e4b369",
    "consumer_uuid":"8871494b-bb1f-99e5-8fbe-4b81da935010",
    "money":10.0,
    "type":2,//类型 1为问卷 ，2为群组 
    "number":1
}
```
### 输出
```json
{
    "code": "666",
    "data": 50.0,
    "message": "充值成功"
}
```
### 异常
```json
{
    "code": "20001",
    "data": null,
    "message": "余额不足"
}
```
## 租户通过uuid，通过名字模糊查询所有用户
consumer/queryByName
### 输入
```json
{
    "pageNum":1,
    "pageSize":1,
    "name":"",
    "uuid":"00acc4a1-98f2-fcb1-9f2d-147b69e4b369"
}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "pageNum": 1,
        "pageSize": 1,
        "size": 1,
        "startRow": 1,
        "endRow": 1,
        "total": 1,
        "pages": 1,
        "list": [
            {
                "valid": 1,
                "questionnaire_max": 2,
                "cluster_number": 0,
                "address": "安徽省",
                "tenant_uuid": "00acc4a1-98f2-fcb1-9f2d-147b69e4b369",
                "cluster_max": 3,
                "name": "段佳宁",
                "id": 100,
                "questionnaire_number": 0,
                "uuid": "8871494b-bb1f-99e5-8fbe-4b81da935010",
                "consumer_uuid": "8871494b-bb1f-99e5-8fbe-4b81da935010"
            }
        ],
        "prePage": 0,
        "nextPage": 0,
        "isFirstPage": true,
        "isLastPage": true,
        "hasPreviousPage": false,
        "hasNextPage": false,
        "navigatePages": 8,
        "navigatepageNums": [
            1
        ],
        "navigateFirstPage": 1,
        "navigateLastPage": 1,
        "firstPage": 1,
        "lastPage": 1
    },
    "message": null
}
```
# 用户
## 用户分页查询问卷
questionnaire/getQuestionnaireByName
### 输入
```json
{
    "pageNum":1,
    "pageSize":10,
    "name":"",
    "uuid":"8871494b-bb1f-99e5-8fbe-4b81da935010"
}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "pageNum": 1,
        "pageSize": 10,
        "size": 2,
        "startRow": 1,
        "endRow": 2,
        "total": 2,
        "pages": 1,
        "list": [
            {
                "valid": 1,
                "questionnaire_uuid": "f325081cfd0e460498bd7cdc459f742e",
                "filename": "imageUuid2",
                "createdBy": "8871494b-bb1f-99e5-8fbe-4b81da935010",
                "name": "测试问卷2",
                "link": "待发布",
                "format": "jpg",
                "id": 36,
                "type": "这是一个测试问卷2",
                "uuid": "f325081cfd0e460498bd7cdc459f742e",
                "image_uuid": "imageUuid2",
                "consumer_uuid": "8871494b-bb1f-99e5-8fbe-4b81da935010"
            },
            {
                "valid": 1,
                "questionnaire_uuid": "bc04d26f189e400393aa0579019dd4f4",
                "filename": "imageUuid",
                "createdBy": "8871494b-bb1f-99e5-8fbe-4b81da935010",
                "name": "测试问卷",
                "link": "待发布",
                "format": "jpg",
                "id": 33,
                "type": "这不是一个测试问卷",
                "uuid": "bc04d26f189e400393aa0579019dd4f4",
                "image_uuid": "imageUuid",
                "consumer_uuid": "8871494b-bb1f-99e5-8fbe-4b81da935010"
            }
        ],
        "prePage": 0,
        "nextPage": 0,
        "isFirstPage": true,
        "isLastPage": true,
        "hasPreviousPage": false,
        "hasNextPage": false,
        "navigatePages": 8,
        "navigatepageNums": [
            1
        ],
        "navigateFirstPage": 1,
        "navigateLastPage": 1,
        "firstPage": 1,
        "lastPage": 1
    },
    "message": "查询成功"
}
```
## 用户创建问卷
questionnaire/create
### 输入
```json
{
    "name":"测试问卷2",
    "createdBy":"8871494b-bb1f-99e5-8fbe-4b81da935010",
    "image_uuid":"imageUuid",
    "type":"这是一个测试问卷2",
    "question":[
        {
            "questionContent":"第一题",
            "type":1,
            "opts":[
                {
                    "opt":"A"
                },{
                    "opt":"B"
                },{
                    "opt":"C"
                },{
                    "opt":"D"
                },{
                    "opt":"E"
                }
            ]
        },
        {
            "questionContent":"第二题",
            "type":2
        }
    ]

}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "name": "测试问卷2",
        "createdBy": "8871494b-bb1f-99e5-8fbe-4b81da935010",
        "image_uuid": "imageUuid",
        "type": "这是一个测试问卷2",
        "question": [
            {
                "questionContent": "第一题",
                "type": 1,
                "opts": [
                    {
                        "opt": "A"
                    },
                    {
                        "opt": "B"
                    },
                    {
                        "opt": "C"
                    },
                    {
                        "opt": "D"
                    },
                    {
                        "opt": "E"
                    }
                ],
                "uuid": "22035cf4e8ce4eb7a5376933b2a9e16f",
                "valid": 1,
                "optiona": "A",
                "optionb": "B",
                "optionc": "C",
                "optiond": "D",
                "optione": "E"
            },
            {
                "questionContent": "第二题",
                "type": 2,
                "uuid": "ac4e05bda7d24ef4902b24e172e6bd3c",
                "valid": 1
            }
        ],
        "uuid": "f325081cfd0e460498bd7cdc459f742e",
        "link": "待发布",
        "valid": 1
    },
    "message": "创建成功"
}
```
## 用户批量创建答者
answerer/createAnswererBatch
### 输入
```json
{
    "uuid":"8871494b-bb1f-99e5-8fbe-4b81da935010",
    "answererList":[
        {
            "username":"100@qq.com",
            "password":"1",
            "phone":"11111111111",
            "name":"测试答者1"
        },
        {
            "username":"101@qq.com",
            "password":"1",
            "phone":"2222222",
            "name":"测试答者2"
        }
    ]
}
```
### 输出1
```json
{
    "code": "666",
    "data": [],
    "message": "创建成功"
}
```
### 输出2
```json
{
    "code": "666",
    "data": [  //data返回已存在数据 即答者已在用户列表
        {
            "username": "100@qq.com",
            "password": "1",
            "phone": "11111111111",
            "name": "测试答者1"
        },
        {
            "username": "101@qq.com",
            "password": "1",
            "phone": "2222222",
            "name": "测试答者2"
        }
    ],
    "message": "读取成功"
}
```
## 通过问卷uuid，群组uuid，修改问卷起止时间
questionnaire/changeTime
### 输入
```json
{
    "questionnaire_uuid":"bc04d26f189e400393aa0579019dd4f4",
    "cluster_uuid":"e6375536-8718-96dc-3fce-1f2b1d5a846b",
    "start_date":"2022-11-22 19:46:18",
    "end_date":"2022-11-27 19:46:20"
}
```
### 输出
```json
{
    "code": "666",
    "data": null,
    "message": "修改成功"
}
```
## 用户通过uuid，名字模糊查询问卷
questionnaire/getQuestionnaireByName
### 输入
```json
{
    "pageNum":1,
    "pageSize":1,
    "name":"",
    "uuid":"131564fsdf"
}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "pageNum": 1,
        "pageSize": 1,
        "size": 1,
        "startRow": 1,
        "endRow": 1,
        "total": 1,
        "pages": 1,
        "list": [
            {
                "questionnaire_uuid": "bc04d26f189e400393aa0579019dd4f4",
                "endDate": "2022-11-22T11:23:54.000+0000",
                "link": "待发布",
                "format": "jpg",
                "type": "这是一个问卷",
                "uuid": "bc04d26f189e400393aa0579019dd4f4",
                "quesionnaire_uuid": "bc04d26f189e400393aa0579019dd4f4",
                "consumer_uuid": "131564fsdf",
                "valid": 1,
                // 封面照片
                "filename": "image",
                "name": "测试问卷",
                "id": 33,
                "startDate": "2022-11-22T11:23:54.000+0000",
                "image_uuid": "image"
            }
        ],
        "prePage": 0,
        "nextPage": 0,
        "isFirstPage": true,
        "isLastPage": true,
        "hasPreviousPage": false,
        "hasNextPage": false,
        "navigatePages": 8,
        "navigatepageNums": [
            1
        ],
        "navigateFirstPage": 1,
        "navigateLastPage": 1,
        "lastPage": 1,
        "firstPage": 1
    },
    "message": "查询成功"
}
```
 ## 用户通过uuid查询群组
group/queryByName
### 输入
```json
{
    "name":"",
    "pageNum":1,
    "pageSize":2,
    "uuid":"8871494b-bb1f-99e5-8fbe-4b81da935010"
}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "pageNum": 1,
        "pageSize": 2,
        "size": 1,
        "startRow": 1,
        "endRow": 1,
        "total": 1,
        "pages": 1,
        "list": [
            {
                "valid": 1,
                "created_time": "2022-11-21 23:06:48",
                "number": 5,
                "name": "软件2001班",
                "history_number": 0,
                "group_uuid": "e6375536-8718-96dc-3fce-1f2b1d5a846b",
                "id": 100,
                "uuid": "e6375536-8718-96dc-3fce-1f2b1d5a846b",
                "consumer_uuid": "8871494b-bb1f-99e5-8fbe-4b81da935010"
            }
        ],
        "prePage": 0,
        "nextPage": 0,
        "isFirstPage": true,
        "isLastPage": true,
        "hasPreviousPage": false,
        "hasNextPage": false,
        "navigatePages": 8,
        "navigatepageNums": [
            1
        ],
        "navigateFirstPage": 1,
        "navigateLastPage": 1,
        "firstPage": 1,
        "lastPage": 1
    },
    "message": null
}
```
# 群组
## 检查群组能否删除（删除第一步）
group/queryStatus
### 输入
```json
{
    "uuid":"616c12d1bb1b4cc09720da638d72fc4a"
}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "status": 1 // 1能删
    },
    "message": null
}
```
## 删除群组（修改valid）（删除第二部）
group/delete
### 输入
```json
{
    "uuid":"616c12d1bb1b4cc09720da638d72fc4a"
}
```
### 输出
```json
{
    "code": "666",
    "data": null,
    "message": "删除成功"
}
```
## 用户通过uuid创建群组
group/create
### 输入
```json
{
    "name":"软件2002班",
    "uuid":"8871494b-bb1f-99e5-8fbe-4b81da935010"
}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "name": "软件2003班",
        "uuid": "66f1430500044599b3cf8c5ccadcb10e",
        "createdTime": "2022-11-24T06:17:01.000+0000",
        "number": 0,
        "valid": 1
    },
    "message": "创建成功"
}
```
### 异常
{
    "code": "20001",
    "data": null,
    "message": "次数不足，请通知租户充值"
}
## 通过群组uuid和问卷uuid获取问卷起止时间
questionnaire/getQuestionnaireByUuid
### 输入
```json
{
    "questionnaire_uuid":"bc04d26f189e400393aa0579019dd4f4",
    "cluster_uuid":"e6375536-8718-96dc-3fce-1f2b1d5a846b"
}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "end_date": "2022-12-20T16:00:00.000+0000",
        "valid": 1,
        "questionnaire_uuid": "bc04d26f189e400393aa0579019dd4f4",
        "cluster_uuid": "e6375536-8718-96dc-3fce-1f2b1d5a846b",
        "id": 100,
        "status": 1,
        "start_date": "2022-11-09T16:00:00.000+0000"
    },
    "message": "查询成功"
}
```
## 群组通过uuid查询问卷
questionnaire/getQuestionnaireByCluster
### 输入
```json
{
    "uuid":"e6375536-8718-96dc-3fce-1f2b1d5a846b"
}
```
### 输出
```json
{
    "code": "666",
    "data": [
        {
            "valid": 1,
            "endDate": "2022-11-22 19:23:54",
            "name": "测试问卷",
            "link": "待发布",
            "id": 33,
            "type": "这是一个问卷",
            "uuid": "bc04d26f189e400393aa0579019dd4f4",
            "startDate": "2022-11-22 19:23:54"
        }
    ],
    "message": "查询成功"
}
```
# 答者
## 答者获取待答问卷
answerer/getQuestionnaire
### 输入
```json
{
    "uuid":"2e9c5763-5012-4fe3-4dc3-df18ec54880d"
}
```
### 输出
```json
{
    "code": "666",
    "data": [
        {
            "end_date": "2022-11-30 09:00:02",
            "answerer_uuid": "2e9c5763-5012-4fe3-4dc3-df18ec54880d",
            "filename": "imageUuid",
            "createdBy": "8871494b-bb1f-99e5-8fbe-4b81da935010",
            "format": "jpg",
            "name": "测试问卷",
            "link": "待发布",
            "type": "这不是一个测试问卷",
            "uuid": "bc04d26f189e400393aa0579019dd4f4",
            "start_date": "2022-11-18 02:02:02",
            "status": 1
        }
    ],
    "message": "查询成功"
}
```
## 答者获取加入的群组
answerer/getAnswererGroup
### 输入
```json
{
    "uuid":"2e9c5763-5012-4fe3-4dc3-df18ec54880d"
}
```
### 输出
```json
{
    "code": "666",
    "data": [
        {
            "cluster_name": "试试",
            "number": 1,
            "consumer_name": "段佳宁",
            "uuid": "48f4b5c21ba7463cae2900cb04a8db29"
        }
    ],
    "message": "查询成功"
}
```

## 检测答者是否可删除（删除第一步）
answerer/queryDelete
### 输入
```json
{
    "answerer_uuid":"ebd70f1b-e908-6ff0-1559-9f6034b0cbdc",
    "consumer_uuid":"8871494b-bb1f-99e5-8fbe-4b81da935010"
}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "status": 1
    },
    "message": "查询成功"
}
```
## 删除答者（修改valid状态为0）
answerer/deleteAnswerer
### 输入
```json
{
    "consumer_uuid":"8871494b-bb1f-99e5-8fbe-4b81da935010",
    "answerer_uuid":"ec1691a67dbf45fda13bce4fc3bc6958"
}
```
### 输出
```json
{
    "code": "666",
    "data": null,
    "message": null
}
```
## 检测答者信息是否完整，不完整跳转到补充信息界面
answerer/checkFirst
### 输入
```json
{
    "uuid":"7ddb3e13-7252-7dc3-1eed-217682b932ee"
}
```
### 输出
```json
{
    "code": "666",
    "data": {
        "valid": 1,
        "address": "沈阳市",
        "role": "老师",
        "organization": "沈阳理工",
        "name": "李四",
        "cite": 1,
        "id": 102,
        "tag": "热爱学习",
        "uuid": "7ddb3e13-7252-7dc3-1eed-217682b932ee",
        "age": 18,
        "jump": 0 //是否跳转 1跳转 0不跳转
    },
    "message": "查询成功"
}
```
### 输出2
```json
{
    "code": "666",
    "data": {
        "valid": 1,
        "name": "测试答者1",
        "cite": 0,
        "id": 104,
        "uuid": "77d7d9ae20e649529e70f8170ef52b6a",
        "jump": 1
    },
    "message": "查询成功"
}
```
## 读取excel
answerer/readExcel
### 输入
```json
表单传文件
```
### 输出
```json
{
    "code": "666",
    "data": [
        {
            "address": "河南省",
            "role": "学生",
            "organization": "东北大学",
            "name": "李四",
            "tag": "三次元",
            "age": "18.0"
        },
        {
            "address": "日本省",
            "role": "老师",
            "organization": "西北大学",
            "name": "王五",
            "tag": "二次元",
            "age": "19.0"
        }
    ],
    "message": "读取成功"
}
```
# 图片
## 上传图片返回uuid
image/upload
### 输入
```json
表单上传图片
```
### 输出
```json
{
    "code": "666",
    "data": "d3965f83a2934c1d82528442beb08725",
    "message": "上传成功"
}
```